# Provas

Provas do curso Idevs - Proway. Provas se encontram separadas por branches.

# Lista de Provas

- Prova 1 (prova sprint 1 e 2) - 06/06/22: Branch prova 1. Projeto: Cadastro de computadores.
- Prova 2 (prova sprint 3 parte 1) - 10/06/22: Branch prova 2. Projeto: Consultório Médico

------

</br></br></br></br></br></br></br></br></br></br></br></br></br></br>



# Descritivo Prova 1

## Cadastro de computadores

Programa para cadastro de computadores, registrando dados tais como processador, quantidades de memória RAM e armazenamento, e preço.

## Edição e exclusão

Edite e exclua os registros de computadores.

## Página de estatísticas

Além de incluir e remover computadores, é possível visualizar dados sobre a quantidade de computadores, valor total em estoque, e menores e maiores valores entre os computadores.


# Descritivo Prova 2

# ConsultorioMedico

Projeto para prova 1 da sprint 3 do curso Idevs - Proway

## Paciente e Médico

Recebe informações do Paciente e do Médico.
Utilizam herança da classe abstrata pessoa.

## Emissão de Atestado
Exibe uma emissão de "atestado" caso seja informado um paciente e médico. Função da classe Médico.

## Emissão de laudo

Exibe mensagem sobre o prognóstico conforme o que for setado na função. Função contida na interface Ex

